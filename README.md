# Space Defender

Get the latest stable version from **[itch.io](https://gdelgiud.itch.io/space-defender)**

## Description

An Asteroids-like game for Windows, Linux and MacOSX, made with [Godot Engine](https://godotengine.org/). Destroy wave after wave of asteroids without getting torn to pieces. Watch out for alien scout ships!

## Controls

Space Defender requires a keyboard to be played, no gamepad support at the moment. Menus can be fully navigated without a mouse. These are the default controls, but they can be changed in the *Settings* screen:

+ **Forward**: Up arrow
+ **Rotate clockwise**: Right arrow
+ **Rotate counter-clockwise**: Left arrow
+ **Fire**: Spacebar
+ **Special**: Z
+ **Pause**: Escape

## Requirements

### Playing

No requirements for playing the game. The binary is completely standalone.

### Editing

Space Defender is made with **Godot 2.1.2**. To edit it, [get the editor for your OS of choice](https://downloads.tuxfamily.org/godotengine/2.1.2/), open it, and import the root directory (where *engine.cfg* is located).

### Exporting

Space Defender can be exported from the editor. However, a script is provided to make batch exporting for Windows, Linux and MacOSX easier. You'll need:

+ Bash
+ [Godot 2.1.2 export templates](https://downloads.tuxfamily.org/godotengine/2.1.2/Godot_v2.1.2-stable_export_templates.tpz)
+ Either Godot editor or [Godot server](https://downloads.tuxfamily.org/godotengine/2.1.2/Godot_v2.1.2-stable_linux_server.64.zip) (for exporting from the command line only)

The file *build.sh* located in the project's root directory makes exporting for all supported platforms from the command line easier. It assumes Godot's binary is in your PATH, under any of these names (in this order):

+ godot_server
+ godot_headless
+ godot

However, you can provide the path to the binary if you like.

You'll also need to set up Godot's export templates. Check [Godot's documentation](https://godot.readthedocs.io/en/stable/tutorials/asset_pipeline/exporting_projects.html) to learn how to set them up.

Here's some examples on how to export Space Defender from the command line

**Listing all available commands and their parameters**

```
./build.sh -h
```

**Listing all available export targets**

```
./build.sh -l
```

**Exporting for all target platforms with an user-provided Godot binary named godot_headless in the project's root directory**

```
./build.sh -a godot_headless
```

**Exporting a 64-bits Linux binary named *space-defender* letting the script find Godot binary in your PATH** (it will be placed under the *bin/linux-x86_64/* directory)

```
./build.sh -t linux-x86_64 space-defender
```

## Assets

All sprites were done by me.

Sounds were downloaded from [Freesound](https://www.freesound.org/).

Fonts used:

+ [Computer Pixel-7](http://www.dafont.com/computer-pixel-7.font)
+ [Pixellari](http://www.dafont.com/pixellari.font)
+ [Upheaval](http://www.dafont.com/upheaval.font)

## Screenshots

![alt text](https://i.imgur.com/6fc4BCy.png "Space Defender")
![alt text](https://i.imgur.com/pfTj3Yb.png "Space Defender")

## License

Copyright (c) 2017 Gustavo Del Giudice

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[![build status](https://gitlab.com/gdelgiud/godot-asteroids/badges/master/build.svg)](https://gitlab.com/gdelgiud/godot-asteroids/commits/master)