#!/bin/bash

# Constants
BUILD_DIRECTORY="bin"
BUILD_CONFIG_DIRECTORY="build_config"
LOG_DIRECTORY="log"

COMMON_EXPORT_FILE="$BUILD_CONFIG_DIRECTORY/export_common.cfg"
EXPORT_FILE_BACKUP="export.cfg.bak"
EXPORT_FILE="export.cfg"

# Prints all available parameters
#
# Parameters:
#   (none)
function print_help {
	echo "Valid parameters:"
	column -t -s \\ <<< '
	-h --help\\Prints this help.
	-t --target (target name) (file name) [OPTIONAL](godot binary)\\Exports for the given target.
	-a --all (file name) [OPTIONAL](godot binary)\\Exports for all targets.
	-l --list\\Lists available targets.
	-c --clean\\Removes generated files.'
}

# Prints all available build targets.
#
# Parameters:
#   (none)
function list_targets {
	ls $BUILD_CONFIG_DIRECTORY | grep 'build_' | sed 's/build_\([a-z0-9_\-]*\).cfg/\1/'
}

# Runs the entire build process for the given target. Creates a $BUILD_DIRECTORY directory, and
# places the resulting binary with the given file name in a subdirectory named after the target.
# Godot's output is logged in $LOG_DIRECTORY/(target name).log
# If Godot's binary path isn't provided, attempts to find a suitable one in $PATH
#
# Parameters:
#   1 - The target name, as listed by list_targets (eg. linux64, win32).
#   2 - The resulting binary file name.
#   3 - The Godot binary to use.
#
# Example:
# build_target "linux64" "game"
# Creates a 64-bits linux binary in $BUILD_DIRECTORY/linux64/game
# Creates a log file in $LOG_DIRECTORY/linux64.log
function build_target {
	target_name="$1"
	file_name="$2"
	godot_binary="$3"
	# Validate inputs
	if [ "$target_name" == "" ]
	then
		echo "Please specify a build target."
		abort
	fi

	if [ "$file_name" == "" ]
	then
		echo "Please specify a file name."
		abort
	fi

	echo "Building target '$target_name'..."

	godot_command=""
	if [ "$godot_binary" != "" ]
	then
		absolute_path=$(realname $3)
		if [ -e "$absolute_path" ]
		then
			godot_command="$absolute_path"
		fi
	fi

	# Find possible Godot commands
	if [ "$godot_command" == "" ]
	then
		declare -a available_commands=("godot_server" "godot_headless" "godot")
		for i in "${available_commands[@]}"
		do
			if command_exists "$i" ;
			then
				godot_command="$i"
				break
			fi
		done

		if [ "$godot_command" == "" ]
		then
			echo "Godot not found in PATH."
			echo "Any of these must be in PATH:"
			for i in "${available_commands[@]}"
			do
				echo " $i"
			done
			abort
		fi
	fi

	echo "Using '$godot_command'."

	prepare_build $target_name $BUILD_DIRECTORY

	# Build
	log_file=$LOG_DIRECTORY/$target_name.log
	target_directory=$target_name
	build_file="$BUILD_CONFIG_DIRECTORY/build_$target_name.cfg"
	godot_target_name=`grep 'godot_target_name' $build_file | sed 's/godot_target_name=//'`
	binary_extension=`grep 'binary_extension' $build_file | sed 's/binary_extension=//'`
	target_file="$BUILD_DIRECTORY/$target_directory/$file_name$binary_extension"
	godot_build_command="$godot_command -export \"$godot_target_name\" \"$target_file\""
	echo "Logging at $log_file."
	echo "Executing..."
	echo "  $godot_build_command"
	$godot_command -export "$godot_target_name" "$target_file"

	if [ ! -e "$target_file" ]
	then
		echo "Build failed. Check the log file for details."
		clean
		abort
	fi

	chmod +x $target_file

	echo "Created $target_file."

	clean
}

# Runs the build process for all targets listed by the --list command.
# Each resulting binary is exported to the given directory, with the given file name, under a
# subdirectory named after the target platform.
#
# Parameters:
#   1 - The resulting binary file name.
#   2 - The Godot binary.
function run_all {
	file_name="$1"
	godot_binary="$2"

    targets=`list_targets`
    for target in $targets
    do
        build_target $target $file_name $godot_binary
    done
    exit
}

# Creates the directories where the resulting binary and log will be placed
#
# Parameters:
#   1 - The target name, as listed by list_targets (eg. linux64, win32).
#   2 - The export directory. It will be created if it doesn't exist.
function prepare_build {
	target_name="$1"
	export_directory="$2"

	bootstrap $target_name

	resulting_directory=$export_directory/$target_name
	mkdir -p $resulting_directory
	echo "Created directory '$resulting_directory'."
	mkdir -p $LOG_DIRECTORY
}

# Creates the needed export.cfg for the given build target
# If it founds a previously created export.cfg file, it backs it up
#
# Parameters:
#   1 - The target name, as listed by list_targets (eg. linux64, win32).
function bootstrap {
	target_name="$1"

	# Validate input
	if [ ! -e $COMMON_EXPORT_FILE ]
	then
		echo "Common export file '$COMMON_EXPORT_FILE' not found."
		abort
	fi

	target_export_file="$BUILD_CONFIG_DIRECTORY/export_$target_name.cfg"
	if [ ! -e "$target_export_file" ]
	then
		echo "Target export file '$target_export_file' not found."
		abort
	fi

	# Back up export file
	if [ -e $EXPORT_FILE ]
	then
		mv $EXPORT_FILE $EXPORT_FILE_BACKUP
		echo "Backed up '$EXPORT_FILE' to '$EXPORT_FILE_BACKUP'."
	else
		echo "$EXPORT_FILE not found, not backing up."
	fi

	cat $COMMON_EXPORT_FILE $target_export_file > $EXPORT_FILE

	if [ ! -e $EXPORT_FILE ]
	then
		echo "Failed to create $EXPORT_FILE."
		abort
	fi

	echo "$EXPORT_FILE created successfully."
}

# Removes generated $EXPORT_FILE, and restores the previous one, if any
function clean {
	echo "Cleaning..."
	rm $EXPORT_FILE 2> /dev/null
	if [ -e $EXPORT_FILE_BACKUP ]
	then
		mv "$EXPORT_FILE_BACKUP" "$EXPORT_FILE"
		echo "Restored '$EXPORT_FILE'."
	fi
}

# Removes generated $EXPORT_FILE, build directory and logs
function clean_all {
	clean
	if [ -d $LOG_DIRECTORY ]
	then
		rm -r $LOG_DIRECTORY
		echo "Removed build log directory."
	fi
	if [ -d $BUILD_DIRECTORY ]
	then
		rm -r $BUILD_DIRECTORY
		echo "Removed binaries directory."
	fi
}

function command_exists {
	hash "$1" 2>/dev/null ;
}

function abort {
	echo "Aborting."
	exit 1
}

# Prints the absolute path for the given file
#
# Parameters:
#   1 - The file name to convert to absolute path
function realname {
	echo "$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
}

if [ "$1" == "-h" -o "$1" == "--help" ]
then
	print_help
	exit
fi

if [ "$1" == "-a" -o "$1" == "--all" ]
then
	run_all $2
	exit
fi

if [ "$1" == "-t" -o "$1" == "--target" ]
then
	build_target $2 $3 $4
	exit
fi

if [ "$1" == "-l" -o "$1" == "--list" ]
then
	echo "Available targets:"
	list_targets
	exit
fi

if [ "$1" == "-c" -o "$1" == "--clean" ]
then
	clean_all
	exit
fi

print_help
exit 1