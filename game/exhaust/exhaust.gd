
extends Node2D

var particles;

func _ready():
	particles = get_node("particles")

func set_enabled(var enabled):
	particles.set_emitting(enabled)
