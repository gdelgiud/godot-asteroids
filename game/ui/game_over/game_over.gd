
extends Panel

signal replay_clicked()
signal main_menu_clicked()
signal quit_clicked()

func show():
	.show()
	get_node("container_vbox/replay_button").grab_focus()

func set_score(score):
	get_node("container_vbox/score_container/score_text").set_text(str(score))

func set_wave(wave):
	get_node("container_vbox/wave_container/wave_text").set_text(str(wave))

# Replay button signal
func on_replay_button_pressed():
	emit_signal("replay_clicked")

# Main menu button signal
func on_main_menu_button_pressed():
	emit_signal("main_menu_clicked")

func on_quit_button_pressed():
	emit_signal("quit_clicked")
