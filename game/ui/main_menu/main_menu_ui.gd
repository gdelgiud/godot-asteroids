
extends ReferenceFrame

onready var start = get_node("container/start")
onready var settings = get_node("container/settings")
onready var version = get_node("version")

func _ready():
	if (Globals.has("spacedefender/version")):
		version.set_text("v%s" % Globals.get("spacedefender/version"))
	start.grab_focus()

func _on_start_pressed():
	var game_scene = scenes.game_scene.instance()
	get_parent().add_child(game_scene)
	get_parent().remove_child(self)

func _on_exit_pressed():
	get_tree().quit()

func _on_settings_pressed():
	var settings_ui = scenes.settings_ui.instance()
	settings_ui.connect("accept", self, "_on_settings_accept")
	settings_ui.connect("cancel", self, "_on_settings_cancel")
	get_parent().add_child(settings_ui)

func _on_settings_cancel():
	settings.grab_focus()

func _on_settings_accept():
	settings.grab_focus()
