
extends Control

var available_texture = preload("res://game/bullet/bullet_available.png")
var unavailable_texture = preload("res://game/bullet/bullet_unavailable.png")
var bullets

func _ready():
	bullets = get_node("container").get_children()

func set_bullets_amount(var amount):
	for i in range(0, bullets.size()):
		if (i >= amount):
			bullets[i].set_texture(unavailable_texture)
		else:
			bullets[i].set_texture(available_texture)
