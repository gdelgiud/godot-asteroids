
extends Control

onready var message_text = get_node("message")
onready var animation = get_node("animation")
onready var message_timer = get_node("message_timer")

func introduce_wave(wave_number):
	var format = tr("LABEL_WAVE_NUMBER")
	var message = format % wave_number
	display_message(message)

func finish_wave():
	display_message("LABEL_WAVE_CLEAR")
	
func display_message(message):
	animation.play("message_in")
	message_text.set_text(message)
	message_timer.start()

func _on_message_timer_timeout():
	animation.play("message_out")
