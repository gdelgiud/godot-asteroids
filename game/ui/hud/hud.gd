
extends HBoxContainer

func set_score(score):
	get_node("score_container/score_text").set_text(str(score))

func set_bonus(bonus):
	get_node("bonus_container/bonus_text").set_text(str(bonus))

func set_level(level):
	get_node("level_container/level_text").set_text(str(level))

func set_remaining_bullets(remaining_bullets):
	get_node("bullets_container/bullets_counter").set_bullets_amount(remaining_bullets)

func fire_special(reload_delay):
	get_node("special_container/special_counter").fire(reload_delay)