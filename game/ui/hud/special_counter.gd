extends Control

onready var reload_progress = get_node("reload_progress")
onready var ready_label = get_node("ready_label")

var is_ready = true
var progress = 0.0
var delay = 0.0

func _ready():
	set_ready()
	set_process(true)

func _process(delta):
	if (!is_ready):
		progress += delta
		reload_progress.set_val(progress)
		if (progress >= delay):
			set_ready()

func fire(reload_delay):
	ready_label.hide()
	progress = 0.0
	delay = reload_delay
	reload_progress.set_max(delay)
	reload_progress.set_value(progress)
	is_ready = false

func set_ready():
	reload_progress.set_val(reload_progress.get_max())
	is_ready = true
	ready_label.show()