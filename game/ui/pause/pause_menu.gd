
extends Panel

signal resume_game()
signal main_menu()
signal quit_game()

onready var settings = get_node("container_vbox/settings_button")

func show():
	.show()
	get_node("container_vbox/resume_button").grab_focus()

func set_score(score):
	get_node("container_vbox/score_container/score_text").set_text(str(score))

func set_wave(wave):
	get_node("container_vbox/wave_container/wave_text").set_text(str(wave))

func _on_settings_cancel():
	settings.grab_focus()

func _on_settings_accept():
	settings.grab_focus()

# Resume button signal
func on_resume_button_pressed():
	emit_signal("resume_game")

# Main menu signal
func on_main_menu_button_pressed():
	emit_signal("main_menu")

# Settings signal
func _on_settings_button_pressed():
	var settings_ui = scenes.settings_ui.instance()
	settings_ui.connect("accept", self, "_on_settings_accept")
	settings_ui.connect("cancel", self, "_on_settings_cancel")
	get_parent().add_child(settings_ui)

# Quit game signal
func on_quit_button_pressed():
	emit_signal("quit_game")
