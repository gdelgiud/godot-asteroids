
extends Panel

signal accept()
signal cancel()

onready var accept = get_node("content/confirm_buttons/accept")
onready var bindings = get_node("content/settings/bindings")
onready var fullscreen = get_node("content/settings/fullscreen")
onready var sounds = get_node("content/settings/sounds")

var current_bindings
var action_names = ["Forward", "Turn right", "Turn left", "Fire", "Fire special", "Pause"]

func _ready():
	fullscreen.set_pressed(global.is_fullscreen)
	sounds.set_pressed(global.sounds_enabled)
	fill_bindings()
	fullscreen.grab_focus()

func fill_bindings():
	current_bindings = global.get_bindings()
	for action in current_bindings.keys():
		var input_control = bindings.get_node(action)
		input_control.set_action(action)
		input_control.set_binding(OS.get_scancode_string(current_bindings[action].scancode))
		input_control.connect("change_binding", self, "change_binding")

func change_binding(action, event):
	bindings.get_node(action).set_binding(OS.get_scancode_string(event.scancode))
	current_bindings[action] = event

func _on_accept_pressed():
	global.is_fullscreen = fullscreen.is_pressed()
	global.sounds_enabled = sounds.is_pressed()
	global.set_bindings(current_bindings)
	global.save_settings()
	emit_signal("accept")
	queue_free()

func _on_cancel_pressed():
	emit_signal("cancel")
	queue_free()
