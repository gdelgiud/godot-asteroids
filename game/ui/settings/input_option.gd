extends HBoxContainer

signal change_binding(action, event)

export var input_name = ""

onready var label = get_node("label")
onready var timer = get_node("input_timer")
onready var button = get_node("button")

var current_binding
var current_action
var is_waiting_for_input

func _ready():
	set_process(true)
	set_input_name(input_name)
	timer.stop()

func _input(event):
	if (is_waiting_for_input):
		if (event.type == InputEvent.KEY):
			# Register the event as handled and stop polling
			get_tree().set_input_as_handled()
			set_process_input(false)
			if (not event.is_action("ui_cancel")):
				is_waiting_for_input = false
				emit_signal("change_binding", current_action, event)

func _process(delta):
	if (is_waiting_for_input):
		var format = "Press [%d]"
		button.set_text(format % round(timer.get_time_left()))

func set_input_name(name):
	label.set_text(name)

func set_action(action):
	current_action = action

func set_binding(binding):
	current_binding = binding
	button.set_text(binding)

func _on_button_pressed():
	set_process_input(true)
	is_waiting_for_input = true
	timer.start()

func _on_input_timer_timeout():
	set_binding(current_binding)
	is_waiting_for_input = false
