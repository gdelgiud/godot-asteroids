
extends Node2D

onready var timer = get_node("timer")

func _ready():
	play("explosion")
	sounds.play_random_explosion()
	if (!timer.is_connected("timeout", self, "_on_timeout")):
		timer.connect("timeout", self, "_on_timeout")
	if (!is_connected("finished", self, "_on_animation_finished")):
		connect("finished", self, "_on_animation_finished")

func _on_timeout():
	timer.disconnect("timeout", self, "_on_timeout")
	queue_free()

func _on_animation_finished():
	hide()
