extends Node

const player = preload("res://game/player/player.tscn")
const bullet = preload("res://game/bullet/bullet.tscn")
const bullet_explosion = preload("res://game/bullet/bullet_explosion.tscn")
const bullet_special = preload("res://game/bullet/bullet_special.tscn")
const player_destroyed = preload("res://game/player/wreck/destroyed_player.tscn")

const enemy = preload("res://game/enemy/enemy_ship.tscn")
const bullet_enemy = preload("res://game/bullet/enemy_bullet.tscn")
const bullet_enemy_explosion = preload("res://game/bullet/enemy_bullet_explosion.tscn")
const enemy_destroyed = preload("res://game/enemy/wreck/destroyed_enemy.tscn")

const explosion_large = preload("res://game/explosions/large_explosion.tscn")
const explosion_medium = preload("res://game/explosions/medium_explosion.tscn")
const explosion_small = preload("res://game/explosions/small_explosion.tscn")

const asteroid_large = preload("res://game/asteroids/big_asteroid.tscn")
const asteroid_medium = preload("res://game/asteroids/medium_asteroid.tscn")
const asteroid_small = preload("res://game/asteroids/small_asteroid.tscn")
const asteroid_debris = preload("res://game/asteroids/debris.tscn")

const game_scene = preload("res://game/scenes/game_scene.tscn")
const main_menu_scene = preload("res://game/scenes/main_menu.tscn")
const settings_ui = preload("res://game/ui/settings/settings_ui.tscn")
