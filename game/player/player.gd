
extends RigidBody2D

signal killed(player, destroyed_ship)
signal bullet_fired(bullet)
signal special_fired(reload_delay)
signal bullet_count_changed(remaining_bullets)

const ROTATE_SPEED = 5
const ACCELERATION = 10.0
const FRICTION = 0.015
const MAX_BULLETS = 5
const BULLET_FIRE_DELAY = 0.10
const BULLET_RELOAD_DELAY = 0.25
const SPECIAL_RELOAD_DELAY = 5.0

onready var cannon = get_node("cannon_position")

var destroyed_ship = null

var is_pressing_up = false
var has_fired_special = false
var is_special_reloading = false

var rotate_direction = 0
var current_bullets = 0
var bullet_fire_timer = 0
var bullet_reload_timer = 0
var special_reload_timer = SPECIAL_RELOAD_DELAY
var dead = false

var exhaust_left
var exhaust_right

func _ready():
	exhaust_left = get_node("exhaust_left")
	exhaust_right = get_node("exhaust_right")
	reset()

func _process(delta):
	handle_input()
	bullet_fire_timer += delta
	bullet_reload_timer += delta
	special_reload_timer += delta
	if (current_bullets >= MAX_BULLETS):
		bullet_reload_timer = 0
	elif (bullet_reload_timer >= BULLET_RELOAD_DELAY):
		set_bullet_amount(current_bullets + 1)
		bullet_reload_timer = 0
		emit_signal("bullet_count_changed", current_bullets)
	exhaust_left.set_enabled(is_pressing_up && !is_dead())
	exhaust_right.set_enabled(is_pressing_up && !is_dead())
	
	# Handle special reload sound
	if (is_special_reloading && special_reload_timer >= SPECIAL_RELOAD_DELAY):
		sounds.play_special_reload()
		is_special_reloading = false

func _fixed_process(delta):
	# Handle rotation
	if (rotate_direction != 0):
		rotate(rotate_direction * ROTATE_SPEED * delta)
	
	# Handle thruster
	if (is_pressing_up):
		apply_impulse(Vector2(0, 0), \
		Vector2(0, -ACCELERATION).rotated(get_rot()))
	
	# Handle special
	if (has_fired_special):
		set_linear_velocity(Vector2(0, 0))
		apply_impulse(Vector2(0, 0), Vector2(0, 500).rotated(get_rot()))
		has_fired_special = false
		
func handle_input():
	rotate_direction = 0
	if (Input.is_action_pressed("left")):
		rotate_direction += 1
	if (Input.is_action_pressed("right")):
		rotate_direction -= 1
	is_pressing_up = Input.is_action_pressed("up")
	
	if (is_pressing_up):
		sounds.play_thruster()
	else:
		sounds.stop_thruster()
	
	if (Input.is_action_pressed("fire")):
		fire_normal()
	
	if (Input.is_action_pressed("fire_special")):
		fire_special()

func fire_normal():
	if (current_bullets > 0 and bullet_fire_timer > BULLET_FIRE_DELAY):
		sounds.play_random_laser()
		set_bullet_amount(current_bullets - 1)
		var bullet = scenes.bullet.instance()
		bullet_fire_timer = 0
		bullet_reload_timer = 0
		fire_bullet(bullet)

func fire_special():
	if (special_reload_timer > SPECIAL_RELOAD_DELAY):
		sounds.play_special()
		has_fired_special = true
		is_special_reloading = true
		special_reload_timer = 0.0
		var bullet = scenes.bullet_special.instance()
		fire_bullet(bullet)
		emit_signal("special_fired", SPECIAL_RELOAD_DELAY)

func fire_bullet(bullet):
		bullet.set_owner(self)
		bullet.set_pos(cannon.get_global_pos())
		cannon.fire()
		bullet.set_rot(get_rot())
		emit_signal("bullet_fired", bullet)

func reset():
	set_bullet_amount(MAX_BULLETS)
	set_linear_velocity(Vector2(0, 0))
	set_angular_velocity(0)
	show()
	
	# Remove destroyed ship
	if (destroyed_ship):
		destroyed_ship.queue_free()
		destroyed_ship = null
	
	set_process(true)
	set_fixed_process(true)
	set_rot(0)
	dead = false

func set_bullet_amount(bullets):
	current_bullets = bullets
	emit_signal("bullet_count_changed", bullets)

func damage(amount):
	kill()

func kill():
	if (!dead):
		# Add shipwreck
		destroyed_ship = scenes.player_destroyed.instance()
		destroyed_ship.set_pos(get_pos())
		destroyed_ship.set_rot(get_rot())
		
		# Add explosion
		var explosion = scenes.explosion_large.instance()
		explosion.set_pos(get_pos())
		get_parent().add_child(explosion)
		
		# Stop thruster sound
		sounds.stop_thruster()
		
		# Disable
		set_linear_velocity(Vector2(0, 0))
		set_angular_velocity(0)
		set_process(false)
		set_fixed_process(false)
		hide()
		
		emit_signal("killed", self, destroyed_ship)
	dead = true

func is_dead():
	return dead
