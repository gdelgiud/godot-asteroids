
extends Node2D

var explosion_magnitude = 20.0

var linear_speeds = []
var angular_speeds = []

func _ready():
	for part in get_children():
		part.apply_impulse(Vector2(0, 0), (part.get_global_pos() - get_global_pos()).normalized() * explosion_magnitude)
		part.set_angular_velocity(0.1 * (randf(0, 1) * 2 - 1))

