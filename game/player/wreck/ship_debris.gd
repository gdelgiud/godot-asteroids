
extends RigidBody2D

func _ready():
	var timer = get_node("timer")
	if (!timer.is_connected("timeout", self, "_on_timer_timeout")):
		timer.connect("timeout", self, "_on_timer_timeout")
	timer.set_wait_time(timer.get_wait_time() * (randf() + 1))
	timer.start()

func _on_timer_timeout():
	var new_explosion = scenes.explosion_medium.instance()
	new_explosion.set_pos(get_pos())
	get_node("timer").disconnect("timeout", self, "_on_timer_timeout")
	get_parent().add_child(new_explosion)
	queue_free()
