
extends Position2D

onready var muzzle = get_node("muzzle")

func _ready():
	muzzle.hide()

func fire():
	muzzle.show()
	muzzle.set_frame(0)
	muzzle.play()

func _on_muzzle_animation_finished():
	muzzle.hide()
	muzzle.stop()
