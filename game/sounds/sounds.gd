extends SamplePlayer

var sample_library = preload("res://game/sounds/library.tres")

var is_playing_thruster_sound = false
var thruster_sound_voice = -1

func _ready():
	set_sample_library(sample_library)
	set_polyphony(10)

func play_thruster():
	if (!is_playing_thruster_sound):
		thruster_sound_voice = sounds.play("thruster")
		is_playing_thruster_sound = true

func stop_thruster():
	if (is_playing_thruster_sound):
		sounds.stop(thruster_sound_voice)
		is_playing_thruster_sound = false

func play_special():
	play("special")

func play_special_reload():
	play("special_reload")

func play_random_laser():
	play_random_sound(["laser01", "laser02", "laser03"])

func play_random_enemy_laser():
	play_random_sound(["enemy_laser01", "enemy_laser02", "enemy_laser03"])

func play_random_explosion():
	play_random_sound(["explosion01", "explosion02", "explosion03"])

func play_random_sound(sounds):
	var sound = sounds[rand_range(0, sounds.size())]
	play(sound)

func play_victory():
	play("victory")