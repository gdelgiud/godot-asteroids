
extends ReferenceFrame

const GROUP_DELETE_ON_RESTART = "delete_on_restart"
const GROUP_DESTROY_TO_END_WAVE = "destroy_to_end_wave"

const BASE_ASTEROID_COUNT = 3
const BONUS_SCORE_TIME_PER_LEVEL = 60 # In seconds
const BONUS_SCORE_PER_LEVEL = 500
const MAX_SPAWN_TIME = 20.0
const MIN_SPAWN_TIME = 5.0

const BOUNDS_WIDTH = 1280
const BOUNDS_HEIGHT = 720

var player
var score = 0
var bonus_time = 0.0
var bonus = 0
var round_bonus = 0
var showed_end_of_wave_message = false
var wave_ended = false

var level = 1

# UI elements
onready var hud = get_node("ui_controller/hud")
onready var pause_menu = get_node("ui_controller/pause_menu")
onready var game_over_menu = get_node("ui_controller/game_over_panel")
onready var game_messages = get_node("ui_controller/game_messages")

# Timers
onready var level_clear_timer = get_node("level_clear_timer")
onready var player_death_timer = get_node("player_death_timer")

onready var enemy_spawner = get_node("enemy_spawner")
onready var bounds = get_node("bounds")

func _ready():
	randomize(true)
	set_process(true)
	bounds.set_bounds(BOUNDS_WIDTH, BOUNDS_HEIGHT)
	restart_game()

func _process(delta):
	if (Input.is_action_pressed("pause")):
		pause_game()
		
	if (wave_ended):
		enemy_spawner.disable()
		if (!showed_end_of_wave_message):
			game_messages.finish_wave()
			sounds.play_victory()
			showed_end_of_wave_message = true
		var delta_bonus = ceil((round_bonus / (level_clear_timer.get_wait_time() * 0.75)) * delta)
		if (bonus > 0):
			if (delta_bonus < bonus):
				bonus -= delta_bonus
				score += delta_bonus
			else:
				score += bonus
				bonus = 0
		if (!level_clear_timer.is_active()):
			round_bonus = bonus
			level_clear_timer.set_active(true)
			level_clear_timer.start()
	else:
		if (bonus_time > 0):
			bonus_time -= delta
		else:
			bonus_time = 0
		bonus = floor(BONUS_SCORE_PER_LEVEL * bonus_time / BONUS_SCORE_TIME_PER_LEVEL)
	
	hud.set_bonus(bonus)
	hud.set_score(score)

func add_asteroids(count):
	for i in range(count):
		var asteroid = scenes.asteroid_large.instance()
		asteroid.spawn(player, get_size())
		on_asteroid_spawned(asteroid)

func on_asteroid_spawned(asteroid):
	asteroid.connect("asteroid_destroyed", self, "_on_asteroid_destroyed")
	asteroid.add_to_group(GROUP_DELETE_ON_RESTART)
	asteroid.add_to_group(GROUP_DESTROY_TO_END_WAVE)
	add_child(asteroid)

func remove_enemy(ship):
	ship.disconnect("enemy_destroyed", self, "_on_enemy_destroyed")
	ship.disconnect("bullet_fired", self, "_on_bullet_fired")
	ship.remove_from_group(GROUP_DESTROY_TO_END_WAVE)
	ship.queue_free()
	check_wave_ended()

func pause_game():
	if (!player.is_dead()):
		sounds.stop_thruster()
		pause_menu.set_score(score)
		pause_menu.set_wave(level)
		pause_menu.show()
		hud.hide()
		game_messages.hide()
		get_tree().set_pause(true)

func unpause_game():
	pause_menu.hide()
	hud.show()
	game_messages.show()
	get_tree().set_pause(false)

func quit_game():
	get_tree().quit()

func restart_game():
	for element in get_tree().get_nodes_in_group(GROUP_DELETE_ON_RESTART):
		element.free()
	create_player()
	hud.show()
	game_messages.show()
	game_over_menu.hide()
	score = 0
	level = 0
	hud.set_score(0)
	player.reset()
	player.set_pos(get_size() / 2)
	next_level()

func back_to_main_menu():
	get_tree().set_pause(false)
	var main_menu_scene = scenes.main_menu_scene.instance()
	get_parent().add_child(main_menu_scene)
	get_parent().remove_child(self)

func next_level():
	wave_ended = false
	level += 1
	bonus_time = BONUS_SCORE_TIME_PER_LEVEL * level
	add_asteroids(BASE_ASTEROID_COUNT + level - 1)
	level_clear_timer.set_active(false)
	hud.set_level(level)
	game_messages.introduce_wave(level)
	enemy_spawner.reset()
	enemy_spawner.max_spawned_enemies = level
	enemy_spawner.set_spawn_delay(get_enemy_spawn_delay(level, MAX_SPAWN_TIME, MIN_SPAWN_TIME))
	showed_end_of_wave_message = false

func show_game_over():
	game_over_menu.set_score(score)
	game_over_menu.set_wave(level)
	game_over_menu.show()

func create_player():
	player = scenes.player.instance()
	player.connect("killed", self, "_on_player_killed")
	player.connect("bullet_count_changed", self, "_on_player_bullet_count_changed")
	player.connect("bullet_fired", self, "_on_bullet_fired")
	player.connect("special_fired", self, "_on_special_fired")
	player.add_to_group("keep_in_field")
	add_child(player)

func remove_player():
	remove_child(player)
	player.disconnect("killed", self, "_on_player_killed")
	player.disconnect("bullet_count_changed", self, "_on_player_bullet_count_changed")
	player.disconnect("bullet_fired", self, "_on_bullet_fired")
	player.disconnect("special_fired", self, "_on_special_fired")

func get_enemy_spawn_delay(level, max_spawn_time, min_spawn_time):
	return (max_spawn_time - min_spawn_time) / level + min_spawn_time

func check_wave_ended():
	wave_ended = get_tree().get_nodes_in_group(GROUP_DESTROY_TO_END_WAVE).empty()

func _on_asteroid_destroyed(asteroid, debris):
	asteroid.disconnect("asteroid_destroyed", self, "_on_asteroid_destroyed")
	asteroid.remove_from_group(GROUP_DESTROY_TO_END_WAVE)
	score += asteroid.get_score()
	hud.set_score(score)
	
	if (!debris.empty()):
		for asteroid in debris:
			on_asteroid_spawned(asteroid)
	check_wave_ended()

func _on_player_death_timeout():
	show_game_over()

func _on_level_clear_timeout():
	next_level()

# Player signal "killed"
func _on_player_killed(player, destroyed_ship):
	enemy_spawner.disable()
	remove_player()
	hud.hide()
	game_messages.hide()
	player_death_timer.start()
	
	destroyed_ship.add_to_group(GROUP_DELETE_ON_RESTART)
	add_child(destroyed_ship)

# Player signal "bullet_count_changed"
func _on_player_bullet_count_changed( remaining_bullets ):
	hud.set_remaining_bullets(remaining_bullets)

# Player signal "bullet_fired"
func _on_bullet_fired( bullet ):
	bullet.add_to_group(GROUP_DELETE_ON_RESTART)
	add_child(bullet)

# Player signal "special_fired"
func _on_special_fired(reload_delay):
	hud.fire_special(reload_delay)

# Game over panel replay signal
func _on_game_over_panel_replay_clicked():
	restart_game()

# Game over panel main menu signal
func _on_game_over_panel_main_menu_clicked():
	back_to_main_menu()

# Game over panel quit signal
func _on_game_over_panel_quit_clicked():
	quit_game()

# Pause menu resume signal
func _on_pause_menu_resume_game():
	unpause_game()

# Pause menu main menu signal
func _on_pause_menu_main_menu():
	back_to_main_menu()

# Pause menu quit signal
func _on_pause_menu_quit_game():
	quit_game()

func _on_enemy_destroyed(ship, wreck):
	score += ship.get_score()
	remove_enemy(ship)
	
	wreck.add_to_group(GROUP_DELETE_ON_RESTART)
	add_child(wreck)

func _on_enemy_spawner_enemy_ship_spawned( ship ):
	ship.connect("enemy_destroyed", self, "_on_enemy_destroyed")
	ship.connect("bullet_fired", self, "_on_bullet_fired")
	ship.target = player
	
	var spawn_x = rand_range(0.0, 1.0)
	var spawn_y = rand_range(0.0, 1.0)
	if (abs(spawn_x - 0.5) < abs(spawn_y - 0.5)):
		spawn_x = round(spawn_x)
	else:
		spawn_y = round(spawn_y)
	
	var spawn_margin = Vector2(60, 60)
	var spawn_position = Vector2(spawn_x, spawn_y) * (get_size() + spawn_margin) - spawn_margin / 2
	
	ship.set_pos(spawn_position)
	ship.add_to_group(GROUP_DELETE_ON_RESTART)
	ship.add_to_group(GROUP_DESTROY_TO_END_WAVE)
	add_child(ship)
