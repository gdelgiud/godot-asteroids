
extends Node

const GROUP_BOUNDS = "keep_in_field"

var bounds

func _ready():
	set_process(true)

func set_bounds(width, height):
	bounds = Vector2(width, height)

func _process(delta):
	for body in get_tree().get_nodes_in_group(GROUP_BOUNDS):
		keep_in_field(body)

func keep_in_field(body):
	var position = body.get_pos()
	var changed = false
	var margin = 30
	if (position.x < -margin):
		position.x = bounds.x + margin
		changed = true
	elif (position.x > bounds.x + margin):
		position.x = -margin
		changed = true
	if (position.y < -margin):
		position.y = bounds.y + margin
		changed = true
	elif (position.y > bounds.y + margin):
		position.y = -margin
		changed = true
	if (changed):
		if (body.has_method("on_exit_field")):
			body.on_exit_field()
		body.set_pos(position)
