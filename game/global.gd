
extends Node

var player_actions = ["up", "left", "right", "fire", "fire_special", "pause"]
var key_bindings = {}

var is_fullscreen
var sounds_enabled = true

func _ready():
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	if err == OK:
		is_fullscreen = config.get_value("display", "fullscreen", false)
		sounds_enabled = config.get_value("sound", "enable", true)
		for action in player_actions:
			key_bindings[action] = config.get_value("bindings", action, InputMap.get_action_list(action)[0])
	else:
		load_default_settings()
	apply_settings()

func get_binding(action):
	return key_bindings[action]
	
func get_bindings():
	var bindings = {}
	for key in key_bindings:
		bindings[key] = key_bindings[key]
	return bindings

func set_bindings(new_bindings):
	key_bindings.clear()
	for key in new_bindings:
		key_bindings[key] = new_bindings[key]

func change_binding(action, scancode):
	# Remove previous binding
	for old_event in InputMap.get_action_list(action):
		InputMap.action_erase_event(action, old_event)
	# Add the new key binding
	InputMap.action_add_event(action, scancode)
	key_bindings[action] = scancode

func save_settings():
	apply_settings()
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	config.set_value("display", "fullscreen", is_fullscreen)
	config.set_value("sound", "enable", sounds_enabled)
	for action in key_bindings.keys():
		config.set_value("bindings", action, key_bindings[action])
	config.save("user://settings.cfg")

func apply_settings():
	OS.set_window_fullscreen(is_fullscreen)
	AudioServer.set_fx_global_volume_scale(1 if sounds_enabled else 0)
	for action in player_actions:
		change_binding(action, key_bindings[action])

func load_default_settings():
	is_fullscreen = false
	sounds_enabled = true
	for action in player_actions:
		var inputEvent = InputEvent()
		inputEvent.type = InputEvent.KEY
		inputEvent.scancode = InputMap.get_action_list(action)[0].scancode
		key_bindings[action] = inputEvent
