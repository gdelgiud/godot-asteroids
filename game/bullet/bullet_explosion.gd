
extends Node2D

onready var timer = get_node("explosion_timer")

func _ready():
	if (!timer.is_connected("timeout", self, "_on_timeout")):
		timer.connect("timeout", self, "_on_timeout")

func _on_timeout():
	timer.disconnect("timeout", self, "_on_timeout")
	queue_free()
