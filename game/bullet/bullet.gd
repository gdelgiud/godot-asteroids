
extends Area2D

export var speed = 700
export var lifetime = 1.0
export var health = 1
export(String) var explosion

var owner
var destroyed = false

onready var timer = get_node("bullet_timer")

func _ready():
	timer.set_wait_time(lifetime)
	timer.start()
	if (!timer.is_connected("timeout", self, "_on_bullet_timeout")):
		timer.connect("timeout", self, "_on_bullet_timeout")
	set_process(true)

func _process(delta):
	translate(Vector2(0, -speed * delta).rotated(get_rot()))

func _on_bullet_timeout():
	destroy()

func destroy():
	if (!destroyed):
		var new_explosion = scenes[explosion].instance()
		new_explosion.set_pos(get_global_pos())
		get_parent().add_child(new_explosion)
		set_process(false)
		destroyed = true
		timer.disconnect("timeout", self, "_on_bullet_timeout")
		queue_free()

func set_owner(new_owner):
	owner = new_owner

func damage(amount):
	health -= amount
	if (health <= 0):
		destroy()

func collide(object):
	if (owner != object && object.has_method("damage") and !destroyed):
		object.damage(1)
		damage(1)
	
func _on_bullet_body_enter( body ):
	collide(body)

func _on_bullet_area_enter( area ):
	collide(area)