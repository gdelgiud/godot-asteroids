
extends "res://game/asteroids/asteroid.gd"

func get_max_speed():
	return 30

func get_score():
	return 10

func get_next_asteroid():
	return scenes.asteroid_medium

func get_explosion():
	return scenes.explosion_large