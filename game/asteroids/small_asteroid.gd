
extends "res://game/asteroids/asteroid.gd"

func get_max_speed():
	return 90

func get_score():
	return 30

func get_next_asteroid():
	return null

func get_explosion():
	return scenes.explosion_small