
extends "res://game/asteroids/asteroid.gd"

func get_max_speed():
	return 60

func get_score():
	return 20

func get_next_asteroid():
	return scenes.asteroid_small

func get_explosion():
	return scenes.explosion_medium