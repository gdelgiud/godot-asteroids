
extends Area2D

signal asteroid_destroyed(asteroid, debris)

var speed = Vector2(0, 0)
var destroyed = false

func _ready():
	connect("body_enter", self, "_on_collide")
	add_to_group("keep_in_field")
	set_process(true)

func _process(delta):
	translate(speed * delta)
	if (destroyed):
		destroy()

func _on_collide(body):
	if (body.is_visible() and body.has_method("damage")):
		damage(1)
		body.damage(1)

func spawn(player, area):
	var player_pos = player.get_pos()
	var new_position = player_pos
	while ((new_position - player_pos).length() < 200):
		new_position = Vector2(rand_range(0, area.x), rand_range(0, area.y))
	set_pos(new_position)
	set_random_properties()

func has_next_asteroid():
	return get_next_asteroid() != null

func get_max_speed():
	return 0

func get_next_asteroid():
	pass

func get_explosion():
	return scenes.explosion_large

func get_score():
	return 0

func damage(amount):
	destroyed = true

func destroy():
	var spawned_asteroid_count = 3
	var spawned_asteroids = Array()
	var next_asteroid_scene = get_next_asteroid()
	if (has_next_asteroid()):
		for i in range(spawned_asteroid_count):
			var new_asteroid = next_asteroid_scene.instance()
			var position = get_pos()
			position.x = position.x + rand_range(-20, 20)
			position.y = position.y + rand_range(-20, 20)
			new_asteroid.set_pos(position)
			new_asteroid.set_random_properties()
			spawned_asteroids.append(new_asteroid)
	emit_signal("asteroid_destroyed", self, spawned_asteroids)
	explode()
	queue_free()

func explode():
	var explosion_scene = get_explosion()
	var new_explosion = explosion_scene.instance()
	var debris = scenes.asteroid_debris.instance()
	new_explosion.set_pos(get_global_pos())
	debris.set_pos(get_global_pos())
	get_parent().add_child(new_explosion)
	get_parent().add_child(debris)

func set_random_properties():
	# Set random sprite orientation
	var direction = randi() % 4
	set_rotd(direction * 90)
	
	# Set random velocity
	speed = Vector2(0, -rand_range(get_max_speed() / 2, get_max_speed())).rotated(rand_range(0, 360))