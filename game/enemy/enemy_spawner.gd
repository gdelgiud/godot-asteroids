extends Node2D

signal enemy_ship_spawned(ship)

onready var spawn_timer = get_node("spawn_timer")

var max_spawned_enemies = 10
var current_spawned_enemies = 0

func disable():
	spawn_timer.stop()

func enable():
	spawn_timer.start()

func reset():
	current_spawned_enemies = 0
	enable()

func set_spawn_delay(delay):
	spawn_timer.set_wait_time(delay)

func _on_spawn_timer_timeout():
	var enemy = scenes.enemy.instance()
	current_spawned_enemies += 1
	emit_signal("enemy_ship_spawned", enemy)
	if (current_spawned_enemies >= max_spawned_enemies):
		disable()
