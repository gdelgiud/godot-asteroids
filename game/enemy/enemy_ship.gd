extends RigidBody2D

signal bullet_fired(bullet)
signal enemy_destroyed(ship, wreck)

const ROTATE_SPEED = 20

var target
var health = 1
var dead = false

var acceleration_time = 0
var acceleration_direction

onready var fire_timer = get_node("fire_timer")
onready var accelerate_timer = get_node("accelerate_timer")

func _ready():
	randomize(true)
	add_to_group("keep_in_field")
	accelerate()
	set_fixed_process(true)

func _fixed_process(delta):
	acceleration_time -= delta
	rotate(get_turning_angle(delta))
	if (acceleration_time >= 0):
		apply_impulse(Vector2(0, 0), Vector2(0, -1).rotated(get_rot()) * 2)
	
func damage(amount):
	health -= amount
	if (!dead && health <= 0):
		dead = true
		var explosion = scenes.explosion_large.instance()
		explosion.set_global_pos(get_global_pos())
		get_parent().add_child(explosion)
		
		# Add shipwreck
		var destroyed_ship = scenes.enemy_destroyed.instance()
		destroyed_ship.set_pos(get_pos())
		destroyed_ship.set_rot(get_rot())
		
		emit_signal("enemy_destroyed", self, destroyed_ship)

func accelerate():
	var rotation = rand_range(0, 2 * PI)
	acceleration_time = accelerate_timer.get_wait_time()
	acceleration_direction = Vector2(1, 0).rotated(rotation)

func get_score():
	return 100

func get_turning_angle(delta):
	var angle = Vector2(0, 1).angle_to(-acceleration_direction) - get_global_rot()
	if (angle > PI):
		angle = 2 * PI - angle
	return clamp(angle, -ROTATE_SPEED, ROTATE_SPEED) * delta

func _on_fire_timer_timeout():
	if (target != null && target.has_method("is_dead") && !target.is_dead()):
		var bullet = scenes.bullet_enemy.instance()
		var diff = (get_global_pos() - target.get_global_pos()).normalized()
		bullet.set_pos(get_global_pos())
		bullet.set_owner(self)
		bullet.set_rot(get_pos().angle_to_point(target.get_pos()))
		emit_signal("bullet_fired", bullet)
		sounds.play_random_enemy_laser()

func _on_accelerate_timer_timeout():
	accelerate()

func _on_enemy_body_enter(body):
	damage(900)
	if (body.has_method("damage")):
		body.damage(1)
